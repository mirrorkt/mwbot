parsoid-rs
============
[![crates.io](https://img.shields.io/crates/v/parsoid.svg)](https://crates.io/crates/parsoid)
[![docs.rs](https://docs.rs/parsoid/badge.svg)](https://docs.rs/parsoid)

The `parsoid` crate is a wrapper around [Parsoid HTML](https://www.mediawiki.org/wiki/Specs/HTML)
that provides convenient accessors for processing and extraction.

See the [full documentation](https://docs.rs/parsoid/) (docs for [main](https://mwbot-rs.gitlab.io/mwbot/parsoid/)).

## Testing
Use the `build_corpus` example to download the first 500 featured articles
on the English Wikipedia to create a test corpus.

The `featured_articles` example will iterate through those downloaded examples
to test the parsing code, clean roundtripping, etc.

## License
parsoid-rs is (C) 2020-2021 Kunal Mehta, released under the GPL v3 or any later version,
see COPYING for details.
