// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.net>

//! Regression test for T338102: query field may not exist
use mwapi_responses::prelude::*;

mod test_client;

#[query(
    generator = "categorymembers",
    gcmtitle = "Category:February",
    gcmnamespace = "1"
)]
struct Response;

#[tokio::test]
async fn no_body() {
    let resp: Response = test_client::test(&Response::params().to_vec())
        .await
        .unwrap();
    // No panic
    assert_eq!(resp.query.pages.len(), 0);
}
