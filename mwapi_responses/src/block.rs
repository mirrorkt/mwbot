/*
Copyright (C) 2023 Count Count <countvoncount123456@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct PageBlockRestriction {
    pub id: u32,
    pub ns: i32,
    pub title: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct PartialBlockRestrictions {
    #[serde(default)]
    pub namespaces: Vec<i32>,
    #[serde(default)]
    pub pages: Vec<PageBlockRestriction>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(untagged)]
pub enum BlockScope {
    Sitewide([(); 0]),
    Partial(PartialBlockRestrictions),
}
