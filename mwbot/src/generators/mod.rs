/*
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

mod categorymember;
mod value;

pub use crate::generators::categorymember::CategoryMemberSort;
use crate::page::InfoResponse;
use crate::{Bot, Error, Page, Result};
pub use categorymember::CategoryMemberType;
use mwapi_responses::prelude::*;
pub use mwbot_derive::Generator;
use std::collections::HashMap;
use tokio::sync::mpsc;
pub use value::ParamValue;

type Receiver = mpsc::Receiver<Result<Page>>;

/// A structure to manage query parameters, but keeps continuation separate
#[derive(Default, Debug)]
struct Params {
    main: HashMap<String, String>,
    continue_: HashMap<String, String>,
}

impl Params {
    /// Merge the main and continuation parameters into one map
    fn merged(&self) -> HashMap<&String, &String> {
        let mut map = HashMap::new();
        map.extend(&self.main);
        map.extend(&self.continue_);
        map
    }
}

/// Recursively get pages that are in given category. Note that it is fully
/// possible to get pages multiple times if they're present in multiple
/// categories.
///
/// To prevent infinite loops, the generator keeps track of categories seen
/// and will not recurse through it multiple times.
pub fn categorymembers_recursive(bot: &Bot, title: &str) -> Receiver {
    let (tx, rx) = mpsc::channel(50);
    let title = title.to_string();
    let bot = bot.clone();
    tokio::spawn(async move {
        // Categories we've already seen
        let mut seen = vec![];
        // Categories that are pending
        let mut pending = vec![title];
        while let Some(category) = pending.pop() {
            // Mark as having seen it to stop loops
            seen.push(category.to_string());
            #[allow(deprecated)]
            let mut gen = categorymembers(&bot, &category);
            while let Some(page) = gen.recv().await {
                if let Ok(page) = &page {
                    if page.is_category()
                        && !seen.contains(&page.title().to_string())
                        && !pending.contains(&page.title().to_string())
                    {
                        pending.push(page.title().to_string());
                    }
                }
                if tx.send(page).await.is_err() {
                    // Receiver hung up, just abort
                    return;
                }
            }
        }
    });
    rx
}

/// Get pages that are members of a category
///
/// See [API documentation](https://www.mediawiki.org/wiki/API:Categorymembers) for more details.
#[derive(Generator)]
#[params(generator = "categorymembers", gcmlimit = "max")]
pub struct CategoryMembers {
    /// Title of the category
    #[param("gcmtitle")]
    title: String,
    /// Get results from pages in these namespaces
    #[param("gcmnamespace")]
    namespace: Option<Vec<u32>>,
    /// Direction to get results in
    #[param("gcmdir")]
    dir: Option<SortDirection>,
    /// Get results that are these category member types
    #[param("gcmtype")]
    type_: Option<CategoryMemberType>,
    /// How to sort category members
    #[param("gcmsort")]
    sort: Option<CategoryMemberSort>,
    #[param("gcmstarthexsortkey")]
    starthexsortkey: Option<String>,
    #[param("gcmendhexsortkey")]
    endhexsortkey: Option<String>,
    #[param("gcmstartsortkeyprefix")]
    startsortkeyprefix: Option<String>,
    #[param("gcmendsortkeyprefix")]
    endsortkeyprefix: Option<String>,
    // TODO: start, end (need a timestamp type)
}

/// Get pages that are in the given category
#[deprecated(
    since = "0.5.3",
    note = "Use the CategoryMembers::new() builder instead"
)]
pub fn categorymembers(bot: &Bot, title: &str) -> Receiver {
    CategoryMembers::new(title.to_string()).generate(bot)
}

/// Get pages that transclude the given template
///
/// See [API documentation](https://www.mediawiki.org/wiki/API:Embeddedin) for more details.
#[derive(Generator)]
#[params(generator = "embeddedin", geilimit = "max")]
pub struct EmbeddedIn {
    /// Title of the template
    #[param("geititle")]
    title: String,
    /// Get results from pages in these namespaces
    #[param("geinamespace")]
    namespace: Option<Vec<u32>>,
    /// Whether and how to filter redirects
    #[param("geifilterredir")]
    filter_redirect: Option<FilterRedirect>,
    /// Direction to get results in
    #[param("geidir")]
    dir: Option<SortDirection>,
}

/// Get pages that transclude the given template
#[deprecated(
    since = "0.5.3",
    note = "Use the EmbeddedIn::new() builder instead"
)]
pub fn embeddedin(bot: &Bot, title: &str) -> Receiver {
    EmbeddedIn::new(title.to_string()).generate(bot)
}

#[derive(Copy, Clone)]
pub enum FilterRedirect {
    All,
    Nonredirects,
    Redirects,
}

impl ParamValue for FilterRedirect {
    fn stringify(&self) -> String {
        match self {
            Self::All => "all",
            Self::Nonredirects => "nonredirects",
            Self::Redirects => "redirects",
        }
        .to_string()
    }
}

impl Default for FilterRedirect {
    fn default() -> Self {
        Self::All
    }
}

#[derive(Copy, Clone)]
pub enum SortDirection {
    Ascending,
    Descending,
}

impl ParamValue for SortDirection {
    fn stringify(&self) -> String {
        match self {
            Self::Ascending => "ascending",
            Self::Descending => "descending",
        }
        .to_string()
    }
}

impl Default for SortDirection {
    fn default() -> Self {
        Self::Ascending
    }
}

/// Get all pages in a given namespace
///
/// See [API documentation](https://www.mediawiki.org/wiki/API:Allpages) for more details.
#[derive(Generator)]
#[params(generator = "allpages", gaplimit = "max")]
pub struct AllPages {
    /// The namespace to enumerate
    #[param("gapnamespace")]
    namespace: u32,
    /// How to filter redirects
    #[param("gapfilterredir")]
    filter_redirect: Option<FilterRedirect>,
    /// Limit to pages with at least this many bytes.
    #[param("gapminsize")]
    min_size: Option<u64>,
    /// Limit to pages with at most this many bytes.
    #[param("gapmaxsize")]
    max_size: Option<u64>,
    /// The page title to start enumerating from.
    #[param("gapfrom")]
    from: Option<String>,
    /// The page title to stop enumerating at.
    #[param("gapto")]
    to: Option<String>,
    /// The title prefix for filtering.
    #[param("gapprefix")]
    prefix: Option<String>,
}

/// Get all pages in a given namespace
#[deprecated(since = "0.5.3", note = "Use the AllPages::new() builder instead")]
pub fn allpages(
    bot: &Bot,
    namespace: u32,
    filter_redirect: FilterRedirect,
) -> Receiver {
    AllPages::new(namespace)
        .filter_redirect(filter_redirect)
        .generate(bot)
}

#[derive(Generator)]
#[params(generator = "search", gsrlimit = "max")]
pub struct Search {
    #[param("gsrsearch")]
    search: String,
    #[param("gsrnamespace")]
    namespace: Option<Vec<u32>>,
    #[param("gsroffset")]
    offset: Option<u64>,
    #[param("gsrqiprofile")]
    qi_profile: Option<String>,
    #[param("gsrwhat")]
    what: Option<SearchWhat>,
    #[param("gsrinterwiki")]
    interwiki: Option<bool>,
    #[param("gsrenablerewrites")]
    enable_rewrites: Option<bool>,
}

pub enum SearchWhat {
    Nearmatch,
    Text,
    Title,
}

impl ParamValue for SearchWhat {
    fn stringify(&self) -> String {
        match self {
            Self::Nearmatch => "nearmatch",
            Self::Text => "text",
            Self::Title => "title",
        }
        .to_string()
    }
}

/// Derivable trait that implements a straightforward builder to construct
/// API parameters to generate a list of pages. Using the MediaWiki API's
/// generator feature, it preloads basic metadata about pages to speed up
/// initial processing.
pub trait Generator: Sized {
    /// Map of API parameters
    fn params(&self) -> HashMap<&'static str, String>;

    /// Start the generator and get a receiver back to get
    /// a list of pages back asynchronously
    fn generate(self, bot: &Bot) -> Receiver {
        #[allow(deprecated)]
        generator(
            bot,
            // FIXME: sync params signature for generator()
            self.params()
                .into_iter()
                .map(|(k, v)| (k.to_string(), v))
                .collect(),
        )
    }
}

/// Get a list of pages using an API generator. `params` should contain the
/// name of the generator, e.g. `"generator": "allpages"`, and any g-prefixed
/// filtering parameters, e.g. `"gapnamespace: "2"`.
#[deprecated(since = "0.5.3", note = "Use the Generator trait/macro instead")]
pub fn generator(bot: &Bot, params: HashMap<String, String>) -> Receiver {
    let (tx, rx) = mpsc::channel(50);
    let bot = bot.clone();
    tokio::spawn(async move {
        let mut params = Params {
            main: params,
            ..Default::default()
        };
        loop {
            let resp: InfoResponse =
                match bot.api.query_response(params.merged()).await {
                    Ok(resp) => resp,
                    Err(err) => {
                        match tx.send(Err(Error::from(err))).await {
                            Ok(_) => break,
                            // Receiver hung up, just abort
                            Err(_) => return,
                        }
                    }
                };
            params.continue_ = resp.continue_.clone();
            for item in resp.into_items() {
                let page = bot.page(&item.title).map(|page| {
                    // unwrap: We just created the page, it's impossible for
                    // another thread to be trying to set metadata
                    page.info.set(item).unwrap();
                    page
                });
                if tx.send(page).await.is_err() {
                    // Receiver hung up, just abort
                    return;
                }
            }
            if params.continue_.is_empty() {
                // All done
                break;
            }
        }
    });
    rx
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::testwp;

    #[tokio::test]
    async fn test_categorymembers() {
        let bot = testwp().await;
        let mut members =
            CategoryMembers::new("Category:!Requests".to_string())
                .generate(&bot);
        while let Some(page) = members.recv().await {
            let page = page.unwrap();
            // This page is the last one, so it should require at least one continuation
            if page.title() == "Category:Unsuccessful requests for permissions"
            {
                assert!(true);
                return;
            }
        }

        panic!("Unable to find the page");
    }

    #[tokio::test]
    async fn test_categorymembers_recursive() {
        let bot = testwp().await;
        let mut members = categorymembers_recursive(&bot, "Category:Mwbot-rs");
        let mut found = false;
        while let Some(page) = members.recv().await {
            if page.unwrap().title() == "Mwbot-rs/Categorized depth 1" {
                found = true;
            }
        }
        // We are also testing that this generator runs to completion and does
        // not get trapped in an infinte loop
        assert!(found, "Found depth 1 page");
    }

    #[tokio::test]
    async fn test_embeddedin() {
        let bot = testwp().await;
        let mut embeddedin =
            EmbeddedIn::new("Template:1x".to_string()).generate(&bot);
        let mut count = 0;
        while let Some(page) = embeddedin.recv().await {
            page.unwrap();
            count += 1;
            if count == 5 {
                break;
            }
        }
        assert_eq!(count, 5);
    }

    #[tokio::test]
    async fn test_allpages() {
        let bot = testwp().await;
        let gen =
            AllPages::new(0).filter_redirect(FilterRedirect::Nonredirects);
        dbg!(gen.params());
        assert_eq!(
            gen.params(),
            HashMap::from([
                ("generator", "allpages".to_string()),
                ("gaplimit", "max".to_string()),
                ("gapnamespace", "0".to_string()),
                ("gapfilterredir", "nonredirects".to_string()),
            ])
        );
        let mut pages = gen.generate(&bot);
        let mut count = 0;
        while let Some(page) = pages.recv().await {
            page.unwrap();
            count += 1;
            if count == 5 {
                break;
            }
        }
        assert_eq!(count, 5);
    }

    #[test]
    fn test_generator_boolean() {
        let gen = Search::new("foo".to_string()).interwiki(true);
        assert!(gen.params().contains_key("gsrinterwiki"));
        let gen = gen.interwiki(false);
        assert!(!gen.params().contains_key("gsrinterwiki"));
    }
}
