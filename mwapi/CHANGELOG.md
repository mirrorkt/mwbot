## 0.5.0 / 2023-04-02
* Increase MSRV to 1.63.

## 0.5.0-alpha.3 / 2023-02-20
* [BREAKING] Use own `Error` type instead of `mwapi_errors`.
* Remove the unstable-combining feature.

## 0.5.0-alpha.2 / 2023-02-01
* Improve documentation.

## 0.5.0-alpha.1 / 2022-01-30
* Add support for file uploads ([T319098](https://phabricator.wikimedia.org/T319098)).
  This requires the `upload` feature to be enabled and is incompatible with WASM.
* [BREAKING] Upgrade `mwapi_errors` to 0.3.0-alpha.1

## 0.4.3 / 2022-12-01
* Mark the `Authorization` header as sensitive

## 0.4.2 / 2022-11-07
* Add support for the `wasm32` architecture.

## 0.4.1 / 2022-10-01
* Git repository moved to [Wikimedia GitLab](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot).
* Issue tracking moved to [Wikimedia Phabricator](https://phabricator.wikimedia.org/project/profile/6182/).

## 0.4.0 / 2022-09-19
* Increase MSRV to 1.60.

## 0.4.0-alpha.2 / 2022-01-04
* Params can now be specified using a BTreeMap.
* Failed requests are now logged in the debug log.
* [UNSTABLE] Combine compatible API GET requests when possible. This feature
  is behind a `unstable-combining` feature flag.
  Fixes [#51](https://gitlab.com/mwbot-rs/mwbot/-/issues/51).
* [BREAKING] Update mwapi_errors to 0.2.0-alpha.1

## 0.4.0-alpha.1 / 2021-12-24
* [BREAKING] Remove nonfunctional "interactive" builder, use the default
  instead (it's functionally equivalent).
* [BREAKING] Drop `get_` prefix from various function names in accordance with
  the Rust library API guidelines.
* [BREAKING] `post()` deserializes to any type. Use `post_value()` if you want
  to receive a `serde_json::Value`.
* Use `tracing` for logging. Fixes [#6](https://gitlab.com/mwbot-rs/mwbot/-/issues/6).
* Add bridge for executing queries generated using `mwapi_responses`.

## 0.3.1 / 2021-11-19
* Log API warnings. Fixes [#15](https://gitlab.com/mwbot-rs/mwbot/-/issues/15).

## 0.3.0 / 2021-11-14
* Automatically retry maxlag and readonly errors. The amount of retries can be
  configured by setting a retry_limit in the builder. By default, the limit is
  ten. Fixes [#7](https://gitlab.com/mwbot-rs/mwbot/-/issues/7).
* Parameters can now be specified using the following types, where P and Q are
  both Display. Fixes [#24](https://gitlab.com/mwbot-rs/mwbot/-/issues/24).
  * `Vec<(P, Q)>`
  * `&Vec<(P, Q)>`
  * `&[(P, Q)]`
  * `&[(P, Q); const LENGTH: usize]`
  * `HashMap<P, Q>`
  * `&HashMap<P, Q>`

## 0.3.0-alpha.6 / 2021-11-07
* Add `Client::http_client()`, which allows getting a reference to the
  underlying `reqwest::Client` for making requests directly or sharing with another
  API client (like Parsoid).

## 0.3.0-alpha.5 / 2021-11-01
* Also switch `post_with_token`'s parameter type to use `Display` rather than
  `AsRef<str>`.
* [BREAKING] `Client::get()` no longer specifies a specific return type, the
  caller must specify one that implements `serde::Deserialize`. The existing
  behavior of returning `serde_json::Value` is now available via `get_value()`.
* Require a minimum of Rust 1.56.

## 0.3.0-alpha.4 / 2021-10-20
* Make it cheaper to clone `mwapi::Client`.

## 0.3.0-alpha.3 / 2021-10-18
* Parameters can now implement `Display` rather than indirectly via 
  `AsRef<str>`. Hopefully this is more convenient for users.
* Use new `mwapi_errors` crate for the Error type.

## 0.3.0-alpha.2 / 2021-10-14
* [BREAKING] `mwapi::Builder` no longer implements `Default`.
* [BREAKING] Remove `bot_builder`, users should use `mwbot` for that instead.
* Use [`assert=user`](https://www.mediawiki.org/wiki/API:Assert) when possible.

## 0.3.0-alpha.1 / 2021-10-13
* Initial alpha release as part of mwbot project
